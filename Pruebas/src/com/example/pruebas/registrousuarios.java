package com.example.pruebas;

import android.animation.ArgbEvaluator;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class registrousuarios extends  Activity implements OnClickListener{
	EditText edit1;
	EditText edit2;
	EditText edit3;
	EditText edit4;
	Button buttonRegistrar;
	TextView tv1;
	Button buttonAtras;
	TextView Username;
	TextView Nombre;
	TextView Apellido;
	TextView Edad;
	public registrousuarios(){
		
	}
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registrousuarios);
        edit1=(EditText)findViewById(R.id.editTextUsername);
		edit2=(EditText)findViewById(R.id.editTextNombre);
		edit3=(EditText)findViewById(R.id.editTextApellido);
		edit4=(EditText)findViewById(R.id.editTextEdad);
		buttonRegistrar=(Button)findViewById(R.id.RealizarRegistro);
		tv1=(TextView)findViewById(R.id.textViewRegistroUsuariosMensaje);
		buttonAtras=(Button)findViewById(R.id.buttonAtrasRegistroUsuarios);
		Username=(TextView)findViewById(R.id.verusuariosTextViewUsuarios);
		Nombre=(TextView)findViewById(R.id.textView2);
		Apellido=(TextView)findViewById (R.id.textView3);
		Edad=(TextView)findViewById(R.id.textView4);
		
		buttonAtras.setOnClickListener(this);
		buttonRegistrar.setOnClickListener(this);
		
		Username.setTextColor(Color.BLUE);
		Nombre.setTextColor(Color.BLUE);
		Apellido.setTextColor(Color.BLUE);
		Edad.setTextColor(Color.BLUE);
		buttonAtras.setBackgroundColor(Color.BLUE);
		buttonAtras.setTextColor(Color.WHITE);
		
		buttonRegistrar.setBackgroundColor(Color.BLUE);
		buttonRegistrar.setTextColor(Color.WHITE);
	}
	
	public void Registrar(){
		if(edit1.getText().toString().isEmpty() || edit2.getText().toString().isEmpty()
				|| edit3.getText().toString().isEmpty() || edit4.getText().toString().isEmpty()){
			tv1.setText("Ingresa todos los datos por favor");
		}
		else{
			Registro registro=new Registro();
			registro.SetUsername(edit1.getText().toString());
			registro.SetNombre(edit2.getText().toString());
			registro.SetApellido(edit3.getText().toString());
			registro.SetEdad(Integer.parseInt(edit4.getText().toString()));
			
			if(leerregistros.Pumba.UsuarioYaexiste(registro.GetUsername())){
				tv1.setText("El nombre de usuario ya esta reservado");
			}
			else{
				leerregistros.Pumba.Registros.add(registro);
				tv1.setText("Registro Realizado");
			}
		}
	}
	public void onClick(View arg0){
		switch (arg0.getId()){
		case R.id.RealizarRegistro:
			Registrar();
			break;
		case R.id.buttonAtrasRegistroUsuarios:
			finish();
			break;
		}
	}


}
