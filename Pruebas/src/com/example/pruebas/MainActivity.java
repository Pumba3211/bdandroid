package com.example.pruebas;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.os.Build;

public  class MainActivity extends Activity implements OnClickListener{
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       
    }
    TextView PequeñoRegistro;
    Button RegistrarNuevo;
    Button VerRegistros;
    Button Exit;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        PequeñoRegistro=(TextView)findViewById(R.id.pr);
        PequeñoRegistro.setTextSize(30);
        PequeñoRegistro.setTextColor(Color.BLUE);
        
        
        RegistrarNuevo=(Button)findViewById(R.id.buttonRealizarRegistro);
        VerRegistros=(Button)findViewById(R.id.buttonVerRegistros);
        Exit=(Button)findViewById(R.id.button3Salir);
        
        RegistrarNuevo.setBackgroundColor(Color.BLUE);
        VerRegistros.setBackgroundColor(Color.BLUE);
        Exit.setBackgroundColor(Color.BLUE);
        
        RegistrarNuevo.setTextColor(Color.WHITE);
        VerRegistros.setTextColor(Color.WHITE);
        Exit.setTextColor(Color.WHITE);
        
       RegistrarNuevo.setOnClickListener(this);
       VerRegistros.setOnClickListener(this);
       Exit.setOnClickListener(this);
        
        
        return true;
    }
    public void onClick(View arg0){
    	switch (arg0.getId()){
    	case R.id.buttonRealizarRegistro:
    		Intent int1=new Intent("com.example.pruebas.registrousuarios");
    		startActivity(int1);
    		break;
    	case R.id.buttonVerRegistros:
    		Intent int2=new Intent("com.example.pruebas.leerregistros");
    		startActivity(int2);
    		break;
    	case R.id.button3Salir:
    		finish();
    		break;
    	}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
   

}
