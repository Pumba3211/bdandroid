package com.example.pruebas;

import java.util.ArrayList;

import android.R.anim;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class leerregistros extends Activity implements OnClickListener {
	Button ButtonAtras;
	ListView ListaRegistros;
	TextView registros;
	ArrayList<String> Datos = new ArrayList<String>();

	public static class Pumba {
		public static ArrayList<Registro> Registros = new ArrayList<Registro>();

		public static boolean UsuarioYaexiste(String Username) {
			boolean exists = false;
			for (int i = 0; i < Registros.size(); i++) {
				if (Registros.get(i).GetUsername().equals(Username)) {
					exists = true;
				}
			}
			return exists;
		}
	}

	public void Leerdatos() {
		for (int i = 0; i < Pumba.Registros.size(); i++) {
			String Username = Pumba.Registros.get(i).GetUsername();
			String Nombre = Pumba.Registros.get(i).GetNombre();
			String Apellido = Pumba.Registros.get(i).GetApellido();
			int edad = Pumba.Registros.get(i).GetEdad();
			Datos.add(Username + " " + Nombre + " " + Apellido + " " + edad);
		}
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.leerregistros);
		ButtonAtras = (Button) findViewById(R.id.verusuariosButtonAtras);
		ListaRegistros = (ListView) findViewById(R.id.verusuariosListViewVer);
		registros=(TextView)findViewById(R.id.verusuariosTextViewUsuarios);
		Leerdatos();
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, Datos);
		ListaRegistros.setAdapter(aa);
		ButtonAtras.setOnClickListener(this);
		ButtonAtras.setTextColor(Color.WHITE);
		ButtonAtras.setBackgroundColor(Color.BLUE);
		registros.setTextColor(Color.BLUE);
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.verusuariosButtonAtras:
			finish();
			break;
		}
	}

}
