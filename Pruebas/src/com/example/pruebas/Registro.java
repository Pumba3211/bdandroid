package com.example.pruebas;

import java.util.ArrayList;


public class Registro {
	public static ArrayList<Registro> Registros =new ArrayList();
	private String Username;
	private String Nombre;
	private String Apellido;
	private int Edad;
	
	public void SetUsername(String Username){
		this.Username=Username;
	}
	public void SetNombre(String Nombre){
		this.Nombre=Nombre;
	}
	public void SetApellido(String Apellido){
		this.Apellido=Apellido;
	}
	public void SetEdad(int Edad){
		this.Edad=Edad;
	}
	public String GetUsername(){
		return this.Username;
	}
	public String GetNombre(){
		return this.Nombre;
	}
	public String GetApellido(){
		return this.Apellido;
	}
	public int GetEdad(){
		return this.Edad;
	}
}
