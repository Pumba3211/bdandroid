package com.example.pruebas;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import MetodosHttp.HttpUsuarios;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class modificarusuario extends Activity implements OnClickListener {
	EditText ID;
	EditText Nombre;
	EditText Apellido;
	EditText Correo;
	EditText contrasena;
	Button Modificar;
	Button Atras;
	HttpUsuarios httpusuarios = new HttpUsuarios();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.modificarusuario);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		ID = (EditText) findViewById(R.id.modificarusuarioEditTextID);
		Nombre = (EditText) findViewById(R.id.modificarusuarioEditTextNombre);
		Apellido = (EditText) findViewById(R.id.modificarusuarioEditTextApellido);
		Correo = (EditText) findViewById(R.id.modificarusuarioEditTextCorreo);
		contrasena = (EditText) findViewById(R.id.modificarusuarioEditTextPassword);
		Modificar = (Button) findViewById(R.id.modificarusuarioButtonModificar);
		Atras = (Button) findViewById(R.id.modificarusuarioButtonAtras);
		Modificar.setOnClickListener(this);
		Atras.setOnClickListener(this);
		return true;
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.modificarusuarioButtonModificar:
			Thread nt = new Thread() {
				@Override
				public void run() {
					try {
						final String res;
						res = httpusuarios.ModificarUsuarioGet(Integer
								.parseInt(ID.getText().toString()), Nombre
								.getText().toString(), Apellido.getText()
								.toString(), Correo.getText().toString(),
								contrasena.getText().toString());
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(modificarusuario.this, res,
										Toast.LENGTH_LONG).show();
							}
						});
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			};
			nt.start();
			break;
		case R.id.modificarusuarioButtonAtras:
			finish();
	
		}
		
	}
}
