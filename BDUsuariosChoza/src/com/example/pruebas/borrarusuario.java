package com.example.pruebas;

import android.R.color;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import MetodosHttp.HttpUsuarios;

public class borrarusuario extends Activity implements OnClickListener {

	TextView BorrarMenu;
	TextView tvID;
	Button Borrar;
	Button Atras;
	EditText IDedit;
	TextView Resultado;
	HttpUsuarios HttpUsuarios=new HttpUsuarios();
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.borrarusuario);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		BorrarMenu = (TextView) findViewById(R.id.BorrarUsuarioTexto);
		tvID = (TextView) findViewById(R.id.borrarusuarioTextViewID);
		Borrar = (Button) findViewById(R.id.borrarusuarioButtonBorrar);
		Atras = (Button) findViewById(R.id.borrarusuarioButtonAtras);
		IDedit = (EditText) findViewById(R.id.borrarusuarioEditTextID);
		Resultado = (TextView) findViewById(R.id.borrarusuarioTextViewResultado);
		BorrarMenu.setTextSize(15);
		BorrarMenu.setTextColor(Color.RED);
		tvID.setTextColor(Color.RED);

		Borrar.setBackgroundColor(Color.RED);
		Atras.setBackgroundColor(Color.RED);

		Borrar.setTextColor(Color.WHITE);
		Atras.setTextColor(Color.WHITE);

		Borrar.setOnClickListener(this);
		Atras.setOnClickListener(this);
		return true;
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.borrarusuarioButtonBorrar:
			Thread nt = new Thread() {
				@Override
				public void run() {
					try {
						final String res;
						res = HttpUsuarios.BorrarUsuarioGet(Integer.parseInt(IDedit.getText().toString()));
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(borrarusuario.this, res,
										Toast.LENGTH_LONG).show();
							}
						});
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			};
			nt.start();
			break;
		case R.id.borrarusuarioButtonAtras:
			finish();
		}
	}
}
