package com.example.pruebas;


import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import MetodosHttp.HttpUsuarios;

public class crearusuario extends Activity implements OnClickListener {
	TextView Menu;
	TextView Nombre;
	TextView Apellido;
	TextView Correo;
	TextView Contraseņa;
	TextView Resultado;
	Button Registrar;
	Button Atras;
	EditText NombreEdit;
	EditText ApellidoEdit;
	EditText CorreoEdit;
	EditText ContraseņaEdit;
	HttpUsuarios HttpUsuarios = new HttpUsuarios();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.crearusuario);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		Menu = (TextView) findViewById(R.id.crearusurioMenu);
		Nombre = (TextView) findViewById(R.id.crearUsuarioTextViewNombre);
		Apellido = (TextView) findViewById(R.id.crearUsuarioTextViewApellido);
		Correo = (TextView) findViewById(R.id.crearUsuarioTextViewCorreo);
		Contraseņa = (TextView) findViewById(R.id.crearUsuarioTextViewContrasena);
		Registrar = (Button) findViewById(R.id.crearUsuarioButtonRegistrar);
		Atras = (Button) findViewById(R.id.crearUsuarioButtonRegistrar);
		NombreEdit = (EditText) findViewById(R.id.crearUsuarioNombre);
		ApellidoEdit = (EditText) findViewById(R.id.crearUsuarioApellido);
		CorreoEdit = (EditText) findViewById(R.id.crearUsuarioCorreoElectronico);
		ContraseņaEdit = (EditText) findViewById(R.id.crearUsuarioContrasena);
		Resultado = (TextView) findViewById(R.id.crearUsuariotextViewResultado);
		Registrar.setOnClickListener(this);
		Atras.setOnClickListener(this);
		return true;
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.crearUsuarioButtonRegistrar:
			Thread nt = new Thread() {
				@Override
				public void run() {
					try {
						final String res;
						res = HttpUsuarios.RegistrarUsuarioGet(
								NombreEdit.getText().toString(), ApellidoEdit.getText().toString(),
								CorreoEdit.getText().toString(), ContraseņaEdit.getText().toString());
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(crearusuario.this, res,
										Toast.LENGTH_LONG).show();
							}
						});
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			};
			nt.start();
			break;
		case R.id.crearUsuarioButtonAtras:
			finish();
			break;
		}
	}
}
