package com.example.pruebas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class metodos extends Activity implements OnClickListener{
	  Button CrearUsuario;
	  Button BorrarUsuario;
	  Button ModificarUsuario;
	  Button VerUsuarios;
	  Button Reconfigurar;
	  
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.metodos);
	       
	    }
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.main, menu);	
	        CrearUsuario=(Button)findViewById(R.id.crearUsuarioMetodos);
	        BorrarUsuario=(Button)findViewById(R.id.borrarUsuariometodos);
	        ModificarUsuario=(Button)findViewById(R.id.modificarUsuarioMetodos);
	        VerUsuarios=(Button)findViewById(R.id.verUsuarioMetodos);
	        Reconfigurar=(Button)findViewById(R.id.reconfigurarMetodos); 
	        
	        CrearUsuario.setOnClickListener(this);
	        BorrarUsuario.setOnClickListener(this);
	        ModificarUsuario.setOnClickListener(this);
	        VerUsuarios.setOnClickListener(this);
	        Reconfigurar.setOnClickListener(this);
	        return true;
	    }
	    public void onClick(View arg0){
	    	switch (arg0.getId()){
	    	case R.id.crearUsuarioMetodos:
	    		Intent int1=new Intent("com.example.pruebas.crearusuario");
	    		startActivity(int1);
	    		break;
	    	case R.id.borrarUsuariometodos:
	    		Intent int2=new Intent("com.example.pruebas.borrarusuario");
	    		startActivity(int2);
	    		break;
	    	case R.id.modificarUsuarioMetodos:
	    		Intent int3=new Intent("com.example.pruebas.modificarusuario");
	    		startActivity(int3);
	    		break;
	    	case R.id.verUsuarioMetodos:
	    		Intent int4=new Intent("com.example.pruebas.verusuarios");
	    		startActivity(int4);
	    		break;
	    	case R.id.reconfigurarMetodos:
	    		finish();
	    		break;
	    	}
	    }
}
