package com.example.pruebas;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends Activity implements OnClickListener {

	TextView Adertencia;
	Button Empezar;
	Button Salir;
	EditText Direccion;
	EditText Puerto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		Adertencia = (TextView) findViewById(R.id.ActivityMainAdvertencia);
		Empezar = (Button) findViewById(R.id.ActivityMainEmpezar);
		Salir = (Button) findViewById(R.id.ActivityMainSalir);
		Direccion = (EditText) findViewById(R.id.ipActivityMain);
		Puerto = (EditText) findViewById(R.id.puertoActityMain);
		Empezar.setOnClickListener(this);
		Salir.setOnClickListener(this);
		Adertencia.setTextColor(Color.RED);
		return true;
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.ActivityMainEmpezar:
			if (Direccion.getText().toString().isEmpty()
					|| Puerto.getText().toString().isEmpty()) {
				Adertencia
						.setText("Debes escribir una dirrecion y puerto para poder continuar");
			} else {
				DireccionIp.Ip = Direccion.getText().toString() + ":"
						+ Puerto.getText().toString();
				Intent int1 = new Intent("com.example.pruebas.metodos");
				startActivity(int1);
			}
			break;
		case R.id.ActivityMainSalir:
			finish();
			break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */

}
