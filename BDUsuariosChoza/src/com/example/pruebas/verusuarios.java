package com.example.pruebas;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import MetodosHttp.HttpUsuarios;
public class verusuarios extends Activity implements OnClickListener {
	TextView Usuarios;
	
	ListView ListaUsuarios;
	HttpUsuarios httpusuarios=new HttpUsuarios();
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verusuarios);
        Thread tr = new Thread(){
			@Override
			public void run(){
				final String Resultado = httpusuarios.leer();
				runOnUiThread(
						new Runnable() {
				
							@Override
							public void run() {
									cargarDatos(httpusuarios.obtDatosJSON(Resultado));
							}
						});
			}			
		};
		tr.start();
       Usuarios=(TextView)findViewById(R.id.verusuariosTextViewVer);
      
       
       Usuarios.setTextColor(Color.GREEN);       
     
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);	
        return true;
    }
    public void cargarDatos(ArrayList<String> datos){
		ArrayAdapter<String> adaptador =
			new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datos);
		ListaUsuarios=(ListView)findViewById(R.id.verusuariosListView2);
		ListaUsuarios.setBackgroundColor(Color.GREEN);
		ListaUsuarios.setAdapter(adaptador);
	}
    public void onClick(View arg0){
    	
    } 

}
